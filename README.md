Fritzing-Parts
==============

This is a collection of parts for Fritzing editor (http://fritzing.org/), based on a "core" library.

## Installation

* Clone repository or download ZIPped archive and decompress it somewhere
* Open Fritzing editor
* In _Parts_ window, create a new bin or open existing one (i.e. _MINE_)
* Right-click inside the parts area and choose _Import..._
* Navigate to the directory containing parts you are interested in and open _fzpz_ file


## Contribution

These parts were created from existing files, made by other people. Original authors are listed in parts metadata.


## License

All parts in this repository are distributed under Creative Commons CC-BY 4.0 license.

![http://creativecommons.org/licenses/by-sa/4.0/](http://i.creativecommons.org/l/by-sa/4.0/88x31.png)


## Complete list of parts

### TTL Logic/7400_DIL

Quad NAND gate.

![](https://github.com/kumashiro/Fritzing-Parts/raw/master/images/7400_DIL.png)


### TTL Logic/7446_DIL

Active-low output decoder/driver for 7-segment common-anode LED displays.

![](https://github.com/kumashiro/Fritzing-Parts/raw/master/images/7446_DIL.png)


### TTL Logic/7475_DIL

4-bit wide D Latch with additional, negated outputs.

![](https://github.com/kumashiro/Fritzing-Parts/raw/master/images/7475_DIL.png)


### Analog/NPN Transistor (European)

General NPN transistor with European symbol.

![](https://github.com/kumashiro/Fritzing-Parts/raw/master/images/NPN_Transistor.png)


### Power/7805_TO220

5V DC Voltage Regulator.

![](https://github.com/kumashiro/Fritzing-Parts/raw/master/images/7805_TO220.png)


### Power/7812_TO220

12V DC Voltage Regulator

![](https://github.com/kumashiro/Fritzing-Parts/raw/master/images/7812_TO220.png)

